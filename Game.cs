﻿namespace SOLID
{
    class Game
    {
        private readonly ILogger _logger;
        private readonly IValidation _validator;
        private readonly IGenerator _random;
        private readonly int _find;
        private int _attempts;

        public Game(ILogger logger, IGenerator generator, IValidation validator, Settings settings)
        {
            _logger = logger;
            _validator = validator;
            _random = generator;
            _find = _random.Generate(settings.From, settings.To);
            _attempts = settings.Attempts;
        }

        public void Play()
        {
            int answer;
            bool flag = true;

            do
            {
                answer = _validator.Validate();

                _attempts--;
                if (answer < _find)
                    _logger.Write("Недобор");
                else if (answer > _find)
                    _logger.Write("Перебор");
                else
                {
                    _logger.Write("Правильно!");
                    flag = false;
                }

                if (_attempts <= 0)
                {
                    _logger.Write("Попытки закончились!");
                    flag = false;
                }
            } while (flag);
        }
    }
}
