﻿namespace SOLID
{
    class Program
    {
        static void Main(string[] args)
        {
            IGenerator random = new Randomizer();
            ILogger logger = new Logger();
            IValidation validator = new Validation(logger);

            SettingsManager manager = new(new Serializer(args[0]));

            Game game = new(logger, random, validator, manager.GetSettings());
            game.Play();
        }
    }
}
