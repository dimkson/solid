﻿using System;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace SOLID
{
    class Serializer : ISerializer
    {
        private readonly string _path;

        public Serializer(string path)
        {
            _path = path;
        }

        public async Task SerializeAsync<T>(T obj)
        {
            using FileStream fs = new(_path, FileMode.OpenOrCreate);

            await JsonSerializer.SerializeAsync<T>(fs, obj);
        }

        public async Task<T> DeserializeAsync<T>()
        {
            using FileStream fs = new(_path, FileMode.Open);

            return await JsonSerializer.DeserializeAsync<T>(fs);
        }
    }
}
