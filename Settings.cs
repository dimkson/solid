﻿namespace SOLID
{
    class Settings
    {
        public int Attempts { get; set; }
        public int From { get; set; }
        public int To { get; set; }
    }
}
