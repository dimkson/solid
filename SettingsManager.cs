﻿using System;

namespace SOLID
{
    class SettingsManager
    {
        private readonly ISerializer _serializer;

        public SettingsManager(ISerializer serializer)
        {
            _serializer = serializer;
        }

        public Settings GetSettings()
        {
            Settings settings;
            try
            {
                settings = _serializer.DeserializeAsync<Settings>().Result;
            }
            catch (Exception)
            {
                _serializer.SerializeAsync(new Settings { Attempts = 10, From = 10, To = 100 }).Wait();
                settings = _serializer.DeserializeAsync<Settings>().Result;
            }
            return settings;
        }
    }
}
