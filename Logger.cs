﻿using System;

namespace SOLID
{
    class Logger : ILogger
    {
        public string Read()
        {
            return Console.ReadLine();
        }

        public void Write(string text)
        {
            Console.WriteLine(text);
        }
    }
}
