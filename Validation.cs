﻿namespace SOLID
{
    class Validation : IValidation
    {
        private ILogger _logger;

        public Validation(ILogger logger)
        {
            _logger = logger;
        }

        public int Validate()
        {
            int answer;

            _logger.Write("Введите число");
            bool flag;
            do
            {
                flag = int.TryParse(_logger.Read(), out answer);
                if (!flag) _logger.Write("Некорректный ввод! Попробуй ещё раз!");
            } while (!flag);

            return answer;
        }
    }
}
