﻿using System;

namespace SOLID
{
    class Randomizer : IGenerator
    {
        public int Generate(int minValue, int maxValue)
        {
            Random random = new();
            return random.Next(minValue, maxValue);
        }
    }
}
