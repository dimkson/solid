﻿using System.Threading.Tasks;

namespace SOLID
{
    interface ISerializer
    {
        Task SerializeAsync<T>(T obj);
        Task<T> DeserializeAsync<T>();
    }
}