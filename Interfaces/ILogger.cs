﻿namespace SOLID
{
    interface ILogger : IWriter, IReader { }
}
