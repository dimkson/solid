﻿namespace SOLID
{
    interface IWriter
    {
        void Write(string text);
    }
}
