﻿namespace SOLID
{
    interface IReader
    {
        string Read();
    }
}
