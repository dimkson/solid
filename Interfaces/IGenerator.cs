﻿namespace SOLID
{
    interface IGenerator
    {
        int Generate(int minValue, int maxValue);
    }
}
