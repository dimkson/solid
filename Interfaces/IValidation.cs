﻿namespace SOLID
{
    interface IValidation
    {
        int Validate();
    }
}
